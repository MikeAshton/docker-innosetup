FROM mcr.microsoft.com/windows/servercore:1803
LABEL maintainer="mike.ashton@ascendx.com"

COPY compiler .

# Sets a command or process that will run each time a container is run from the new image.
CMD [ "powershell" ]
